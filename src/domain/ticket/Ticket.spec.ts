import { TicketAPI } from "@/infra/domain"
import { Ticket } from "./Ticket"

const ticketAPI: TicketAPI = {
  id: '7u',
  title: 'awesome name',
  type: 'show',
  releaseDate: 1675595010500,
  description: 'desc',
  price: 10000000000000,
  currency: 'peso argentino',
}

describe('Ticket instance', () => {
  it('ticket props map', () => {
    const ticket = new Ticket(ticketAPI);

    const ticketObject = {
      id: ticket.id,
      title: ticket.title,
      type: ticket.type,
      releaseDate: ticket.releaseDate,
      description: ticket.description,
      price: ticket.price,
      currency: ticket.currency
    }    

    expect(ticketObject).toEqual(ticketAPI);
  })

  it('format release date', () => {
    const ticket = new Ticket(ticketAPI);
    const formattedReleaseDate = ticket.getFormattedReleaseDate();

    expect(formattedReleaseDate).toBe('05/02/2023')
  }) 
})