import { TicketAPI } from '@/infra/domain';

class Ticket {

  private readonly _id: string;
  private readonly _title: string;
  private readonly _type: string;
  private readonly _releaseDate: number;
  private readonly _description: string;
  private readonly _price: number;
  private readonly _currency: string;

  constructor(ticket: TicketAPI) {
    const { id, title, type, releaseDate, description, price, currency } = ticket;
    
    this._id = id;
    this._title = title
    this._type = type;
    this._releaseDate = releaseDate;
    this._description = description;
    this._price = price;
    this._currency = currency;
  }

  get id(): string {
    return this._id;
  }

  get title(): string {
    return this._title;
  }

  get type(): string {
    return this._type;
  }

  get description(): string {
    return this._description;
  }

  get price(): number {
    return this._price;
  }

  get currency(): string {
    return this._currency;
  }

  get releaseDate(): number {
    return this._releaseDate;
  }

  getFormattedReleaseDate() {
    return new Date(this._releaseDate).toLocaleDateString("es-ES", { year: 'numeric', month: '2-digit', day: '2-digit' });
  }

}

export { Ticket }