import styles from './Loading.module.css';

const Loading = () => {
  return <p className={styles.container}>Loading ...</p>
}

export { Loading }