import styles from './FieldDetail.module.css';

type FieldDetailProps = {
  name: string;
  value: string;
  testId?: string;
}

const FieldDetail = ({name, value, testId}: FieldDetailProps) => {
  return (
    <div className={styles.container}>
      <strong>{name}</strong>
      <div data-testid={testId}>{value}</div>
    </div>
  )
}

export { FieldDetail }