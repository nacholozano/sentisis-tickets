import ReactModal from "react-modal";
import style from './Modal.module.css';

const Modal = (props: ReactModal.Props) => {
  return (
    <ReactModal
      {...props}
      shouldCloseOnOverlayClick={true}
      className={style.modal}
    >  
    </ReactModal>
  )
}

export { Modal };