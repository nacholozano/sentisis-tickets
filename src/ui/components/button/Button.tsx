import styles from './Button.module.css';

type ButtonProps = {
  children: string;
  onClick: () => void;
  testId?: string;
}

const Button = ({children, onClick, testId}: ButtonProps) => {
  return (
    <button className={styles.button} onClick={onClick} data-testid={testId}>{children}</button>
  )
}

export { Button }