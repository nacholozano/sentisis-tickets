import { CartItem } from "@/application/tickets/Cart";
import { Button } from "@/ui/components/button/Button";
import { Modal } from "@/ui/components/modal/Modal";
import { useEffect, useState } from "react";
import styles from './TicketsCart.module.css';

type TicketsCartProps = {
  orderedTickets: CartItem[];
  total: number;
}

const TicketsCart = ({orderedTickets, total}: TicketsCartProps) => {

  const [openModal, setOpenModal] = useState(false);
  const [currency, setCurrency] = useState('');

  useEffect(() => {
    setCurrency(orderedTickets[0]?.currency);
  }, [orderedTickets])

  return (
    <div>
      <Button onClick={() => { setOpenModal(true) }} testId='cart-button'>Cart</Button>
      <Modal
        isOpen={openModal}
        onRequestClose={() => { setOpenModal(false) }}
      >
        <h2>Cart</h2>
        {
          orderedTickets.map((ticket: any) => (
            <div key={ticket.id} className={styles.event} data-testid={'cart-row-' + ticket.id}>
              <div data-testid='title'>
                {ticket.title}
              </div>
              <div>
                <span data-testid='units'>{ticket.units}</span> unit/s X <span data-testid='price'>{ticket.price}</span> {currency} = <span data-testid='subtotal'>{ticket.totalPrice}</span> {currency}
              </div>
            </div>
          ))
        }
        <p className={styles.total}>Total: <span data-testid='total'>{total}</span> {currency}</p>
      </Modal>
    </div>
  )
}

export { TicketsCart };