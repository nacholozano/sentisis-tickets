import { CartItem } from "@/application/tickets/Cart";
import { fireEvent, render, screen, waitFor, within } from "@testing-library/react"
import { TicketsCart } from "./TicketsCart"

jest.mock('@/ui/components/modal/Modal');

const orderedTickets: CartItem[] = [
  {
    id: '7u',
    title: 'Ticket 1',
    units: 3,
    price: 10,
    totalPrice: 30,
    currency: 'euro'
  },
  {
    id: '9i',
    title: 'Ticket 2',
    units: 2,
    price: 5,
    totalPrice: 10,
    currency: 'euro'
  },
  {
    id: '2b',
    title: 'Ticket 3',
    units: 1,
    price: 30,
    totalPrice: 30,
    currency: 'euro'
  }
];
const total = 100;

describe('TicketsCart component', () => {
  it('show ticket info', async () => {
    render(
      <TicketsCart orderedTickets={orderedTickets} total={total}/>
    )

    fireEvent.click(screen.getByTestId('cart-button'));

    const row2 = await waitFor(() => screen.findByTestId('cart-row-9i'))
    const ticket = orderedTickets[1];

    expect(within(row2).getByText(ticket.title)).toBeTruthy();
    expect(within(row2).getByText(ticket.units)).toBeTruthy();
    expect(within(row2).getByText(ticket.price)).toBeTruthy();
    expect(within(row2).getByText(ticket.totalPrice)).toBeTruthy();
  })
})