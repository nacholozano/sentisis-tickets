import { TicketItem } from "@/application/tickets/Ticket"
import { fireEvent, render, screen } from "@testing-library/react"
import { TicketDetail } from "./TicketDetail"

jest.mock('@/ui/components/modal/Modal');

const ticket: TicketItem = {
  id: '6y',
  title: 'Ticket title',
  type: 'talk',
  price: 30,
  description: 'lorem ipsum',
  currency: 'euro',
  formattedReleaseDate: '12/10/9294',
  units: 4,
}

describe('TicketDetail component', () => {
  it('print fields', () => {
    render(
      <TicketDetail 
        open={true} 
        ticket={ticket} 
        onClose={() => null}
        onAddTicket={() => null}
      />
    )

    expect(screen.getByText(ticket.title)).toBeTruthy();
    expect(screen.getByText(ticket.type)).toBeTruthy();
    expect(screen.getByText(ticket.description)).toBeTruthy();
  })

  it('add ticket', () => {
    const onClose = jest.fn();
    const onAddTicket = jest.fn();

    render(
      <TicketDetail 
        open={true} 
        ticket={ticket} 
        onClose={onClose}
        onAddTicket={onAddTicket}
      />
    )

    fireEvent.click(screen.getByText('Add'));

    expect(onAddTicket).toHaveBeenCalledWith(ticket, 5);
    expect(onClose).toHaveBeenCalledTimes(1);
  })

})