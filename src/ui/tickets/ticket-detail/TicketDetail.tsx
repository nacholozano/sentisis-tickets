import { TicketItem } from "@/application/tickets/Ticket";
import { Button } from "@/ui/components/button/Button";
import { FieldDetail } from "@/ui/components/field-detail/FieldDetail";
import { Modal } from "@/ui/components/modal/Modal";

type TicketDetailProps = {
  open: boolean;
  ticket: TicketItem;
  onClose: () => void;
  onAddTicket: (ticket: TicketItem, units: number) => void;
}

const TicketDetail = ({open, ticket, onClose, onAddTicket}: TicketDetailProps) => {

  const addTicket = () => {
    onAddTicket(ticket, ticket.units + 1);
    onClose();
  }

  return (
    <Modal
      isOpen={open}
      onRequestClose={() => { onClose() }}
    >   
      <h2>Ticket detail</h2>

      <FieldDetail name='Title' value={ticket.title} testId='title'/>
      <FieldDetail name='Type' value={ticket.type} testId='type'/>
      <FieldDetail name='Description' value={ticket.description} testId='description'/>

      <Button onClick={() => { addTicket() }} testId='add-item-detail-button'>Add</Button>

    </Modal>
  )
}

export { TicketDetail, type TicketDetailProps };