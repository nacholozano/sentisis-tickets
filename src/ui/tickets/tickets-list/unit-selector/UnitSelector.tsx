import { useRef } from "react";
import styles from './UnitSelector.module.css';

type UnitSelectorProps = {
  value?: number;
  onChange: (value: number) => void;
}

const UnitSelector = ({value = 0, onChange}: UnitSelectorProps) => {

  const input = useRef<HTMLInputElement>(null);

  const stopPropagation = (event: any) => { event.stopPropagation() }

  const increase = () => {
    if (!input.current) { return; }

    const currentValue = Number(input.current.value);
    
    const value = currentValue + 1;
    input.current.value = value.toString();

    onChange(value);
  }

  const decrease = () => {
    if (!input.current) { return; }

    const currentValue = Number(input.current.value);

    if (currentValue <= 0) { return; }

    const value = currentValue - 1;
    input.current.value = value.toString();

    onChange(value);
  }

  const handleInputChange = (value: string) => {
    const currentValue = Number(value);

    if (input.current && currentValue < 0) {
      input.current.value = '0';
      onChange(0);
    } else {
      onChange(currentValue);
    }
  }

  return (
    <div onClick={stopPropagation} className={styles.container}>
      <button 
        onClick={() => increase()} 
        className={styles.buttons}
        data-testid='unit-increase'
      >
        +
      </button>

      <input 
        type='number' 
        value={value} 
        ref={input} 
        min={0} 
        onChange={(event) => { handleInputChange(event.target.value) }}
        className={styles.input}
        data-testid='unit-selector-input'
      />

      <button 
        onClick={() => decrease()}
        className={styles.buttons}
        data-testid='unit-decrease'
      >
        -
      </button>
    </div>
  )
}

export { UnitSelector }