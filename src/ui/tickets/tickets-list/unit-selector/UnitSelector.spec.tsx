import { fireEvent, render, screen } from "@testing-library/react"
import { UnitSelector } from './UnitSelector';

const inputTestId = 'unit-selector-input';

describe('UnitSelector component', () => {
  it('Init load with value zero', () => {
    render(<UnitSelector onChange={() => {}}/>);

    expect(screen.getByTestId(inputTestId)).toHaveValue(0);
  })

  it('Load with default value', () => {
    render(<UnitSelector value={5} onChange={() => {}}/>);

    expect(screen.getByTestId(inputTestId)).toHaveValue(5);
  })

  it('Increase number works correctly', async () => {
    const onChange = jest.fn();

    render(<UnitSelector value={0} onChange={onChange}/>);

    fireEvent.click(screen.getByText('+'))
    fireEvent.click(screen.getByText('+'))

    expect(onChange).toHaveBeenCalledWith(2);
    expect(screen.getByTestId(inputTestId)).toHaveValue(2);
  })

  it('Decrease number works correctly', async () => {
    const onChange = jest.fn();

    render(<UnitSelector value={2} onChange={onChange}/>);

    fireEvent.click(screen.getByText('-'))
    fireEvent.click(screen.getByText('-'))

    expect(onChange).toHaveBeenCalledWith(0);
    expect(screen.getByTestId(inputTestId)).toHaveValue(0);

    // Negative numbers are not allowed
    fireEvent.click(screen.getByText('-'));

    // Just called two times because previous clicks on decrease button
    expect(onChange).toHaveBeenCalledTimes(2);
    // Input value remains the same
    expect(screen.getByTestId(inputTestId)).toHaveValue(0);
  })

  it('Write positive value', async () => {
    const onChange = jest.fn();

    render(<UnitSelector value={0} onChange={onChange}/>);

    fireEvent.change(screen.getByTestId(inputTestId), {target: {value: 3}})

    expect(onChange).toHaveBeenCalledWith(3);
  })

  it('Write negative value', async () => {
    const onChange = jest.fn();

    render(<UnitSelector value={3} onChange={onChange}/>);

    fireEvent.change(screen.getByTestId(inputTestId), {target: {value: -6}})

    expect(onChange).toHaveBeenCalledWith(0);
  })

  it('Write invalid value', async () => {
    const onChange = jest.fn();

    render(<UnitSelector value={3} onChange={onChange}/>);

    fireEvent.change(screen.getByTestId(inputTestId), {target: {value: 'string'}})

    expect(onChange).toHaveBeenCalledWith(0);
  })
})