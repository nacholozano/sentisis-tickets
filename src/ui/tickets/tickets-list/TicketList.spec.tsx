import { TicketItem } from "@/application/tickets/Ticket";
import { fireEvent, render, screen, waitFor, within } from "@testing-library/react"
import { TicketsList } from "./TicketsList"

jest.mock('@/ui/components/modal/Modal');

const tickets: TicketItem[] = [
  {
    id: "7p",
    title: "action for happinness",
    type: "talk",
    formattedReleaseDate: "20/10/2021",
    description: "Donec facilisis quis risus ut blandit. Morbi iaculis vel nisi ut cursus. In vel imperdiet odio, imperdiet mollis diam. Nulla vulputate orci arcu, sed tincidunt est tempor et. Aenean at.",
    price: 9,
    currency: "euro",
    units: 0
  },
  {
    id: "1a",
    title: "Johnny Cash tribute",
    type: "show",
    formattedReleaseDate: "23/04/2019",
    description: "Mauris finibus commodo malesuada. Vestibulum porttitor, massa a gravida faucibus, augue velit tristique libero, sit amet scelerisque tortor erat ut velit. Praesent orci tellus, aliquam id felis vitae, laoreet facilisis.",
    price: 15,
    currency: "euro",
    units: 0
  },
  {
    id: "2e",
    title: "vegan for beginners",
    type: "talk",
    formattedReleaseDate: "01/10/2018",
    description: "Curabitur eget magna dui. Nam risus ligula, sagittis eget malesuada eu, blandit eget urna. Sed dignissim ante id quam feugiat, eget dictum libero sollicitudin. Aliquam a urna nec leo efficitur facilisis.",
    price: 8.5,
    currency: "euro",
    units: 0
  },
];

describe('TicketList component', () => {
  it('show ticket info', () => {
    render(<TicketsList tickets={tickets} onUnitsUpdate={() => null}/>)

    const row2 = screen.getByTestId('list-row-1a');
    const ticket = tickets[1];

    expect(within(row2).getByText(ticket.title)).toBeTruthy();
    expect(within(row2).getByText(ticket.type)).toBeTruthy();
    expect(within(row2).getByText(ticket.formattedReleaseDate)).toBeTruthy();
    expect(within(row2).getByText(ticket.price)).toBeTruthy();
  })

  it('emit unitChange from input', () => {
    const unitsUpdate = jest.fn();

    render(<TicketsList tickets={tickets} onUnitsUpdate={unitsUpdate}/>)

    const row2 = screen.getByTestId('list-row-1a');

    fireEvent.change(within(row2).getByTestId('unit-selector-input'), {target: {value: 10}})

    const ticket = tickets[1];

    expect(unitsUpdate).toHaveBeenCalledWith(ticket, 10);
  })

  it('emit unitChange from modal detail',  async() => {
    const unitsUpdate = jest.fn();

    render(<TicketsList tickets={tickets} onUnitsUpdate={unitsUpdate}/>)

    const row2 = screen.getByTestId('list-row-1a');

    fireEvent.click(row2);

    await waitFor(() => screen.findByText('Add'));
    fireEvent.click(screen.getByText('Add'));

    const ticket = tickets[1];

    expect(unitsUpdate).toHaveBeenCalledWith(ticket, 1);
  })
})