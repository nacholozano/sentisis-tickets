import { UnitSelector } from "./unit-selector/UnitSelector";
import styles from './TicketsList.module.css';
import { useState } from "react";
import { TicketDetail } from "../ticket-detail/TicketDetail";
import { TicketItem } from "@/application/tickets/Ticket";

type TicketsListProps = {
  tickets: TicketItem[];
  onUnitsUpdate: (ticket: TicketItem, units: number) => void;
}

const TicketsList = ({tickets, onUnitsUpdate}: TicketsListProps) => {

  const [ticketDetail, setTicketDetail] = useState<TicketItem>();
  const [openTicketDetail, setOpenTicketDetail] = useState<boolean>(false);

  const showDetail = (ticket: TicketItem) => {
    setTicketDetail(ticket); 
    setOpenTicketDetail(true);
  }

  const handleUnitChange = (ticket: TicketItem, units: number) => {
    onUnitsUpdate(ticket, units);
  };

  return (
    <>
      {
        tickets.length
        ? (
            <table className={styles.table}>
              <thead>
                <tr>
                  <th className={styles.th}>Name</th>
                  <th className={styles.th}>Type</th>
                  <th className={styles.th}>Release date</th>
                  <th className={styles.th}>Unit selector</th>
                  <th className={styles.th}>Price</th>
                </tr>
              </thead>
              <tbody>
                {
                  tickets.map((ticket: TicketItem) => (
                    <tr 
                      key={ticket.id} 
                      onClick={() => showDetail(ticket)}
                      className={styles.row}
                      data-testid={'list-row-'+ticket.id}
                    >
                      <td className={styles.td}>{ticket.title}</td>
                      <td className={styles.td}>{ticket.type}</td>
                      <td className={styles.td}>{ticket.formattedReleaseDate}</td>
                      <td className={styles.unitsCell}>
                        <UnitSelector value={ticket.units} onChange={(units) => handleUnitChange(ticket, units)}/>
                      </td>
                      <td className={styles.td}>
                        <span>{ticket.price}</span> {ticket.currency}
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          )
        : <p>No tickets</p>
      }
      
      { 
        ticketDetail && 
        <TicketDetail
          open={openTicketDetail} 
          ticket={ticketDetail} 
          onClose={() => setOpenTicketDetail(false)}
          onAddTicket={handleUnitChange}
        />
      }
    </>
  );
}

export { TicketsList }