import { Application } from "@/application";
import { useEffect, useRef, useState } from "react";
import ReactModal from "react-modal";
import { TicketsCart } from "./tickets/tickets-cart/TicketsCart";
import { TicketsList } from "./tickets/tickets-list/TicketsList";
import styles from './App.module.css';
import { Separator } from "@/ui/components/separator/Separator";
import { TicketItem } from "@/application/tickets/Ticket";
import { Cart } from "@/application/tickets/Cart";
import { Loading } from "@/ui/components/loading/Loading";

ReactModal.setAppElement('body');

function App() {

  const main = useRef<HTMLElement>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [tickets, setTickets] = useState<TicketItem[]>([]);
  const [cartList, setCartList] = useState<Cart>({total: 0, orderedTickets: []});

  const unitsUpdate = (ticket: TicketItem, units: number) => {

    const ticketIndex = tickets.findIndex((item: TicketItem) => item.id === ticket.id);
    tickets[ticketIndex].units = units;
    setTickets([...tickets]);

    const cart = Application.getCart(tickets);
    setCartList(cart);

    Application.saveTickets(cart.orderedTickets);
  }

  useEffect(() => {
    setLoading(true);

    Application.getTickets()
      .then((tickets: TicketItem[]) => {
        setTickets(tickets);

        const cart = Application.getCart(tickets);
        setCartList(cart);
      })
      .finally(() => {
        setLoading(false);
      })
  }, [])

  return (
    <main ref={main} className={styles.main}>
      <h1>Sentisis tickets</h1>
      {
        loading 
        ? <Loading/>
        : <>
            <TicketsList 
              tickets={tickets} 
              onUnitsUpdate={unitsUpdate}
            />

            <Separator/>
          </>
      }     

      {
        cartList.orderedTickets.length
        ? (<TicketsCart orderedTickets={cartList.orderedTickets} total={cartList.total}/>)
        : null
      }
    </main>
  );
}

export default App;
