import { storageService } from "@/infra/services/Storage.service";
import { cartStorageKey } from "../../config";

const getSavedTickets = (): Record<string, number> => {
  return storageService.get(cartStorageKey) || {};
}

export { getSavedTickets };