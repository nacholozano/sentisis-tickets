import { storageService } from "@/infra/services/Storage.service";
import { getSavedTickets } from "./getSavedTickets"

describe('getSavedTicket use case', () => {

  let storageServiceSpy: jest.SpyInstance;

  beforeEach(() => {
    storageServiceSpy = jest.spyOn(storageService, 'get')
  })

  it('Get saved tickets', () => {
    const tickets = { '3r': 7, '1n': 1 };
    storageServiceSpy.mockReturnValue(tickets);

    expect(getSavedTickets()).toEqual(tickets);
  })

  it('No saved tickets', () => {
    storageServiceSpy.mockReturnValue(null);

    expect(getSavedTickets()).toEqual({});
  })

})