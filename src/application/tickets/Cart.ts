interface Cart {
  total: number;
  orderedTickets: CartItem[];
}

interface CartItem {
  id: string;
  title: string;
  units: number;
  price: number;
  totalPrice: number;
  currency: string;
}

export type { Cart, CartItem };