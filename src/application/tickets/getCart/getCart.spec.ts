import { getCart } from "./getCart";

const tickets = [
  {
    id: '2b',
    title: 'Ticket 3',
    type: 'show',
    price: 30,
    description: 'desc 3',
    currency: 'euro',
    formattedReleaseDate: '10/10/2020',
    units: 1,
  },
  {
    id: '7u',
    title: 'Ticket 1',
    type: 'show',
    price: 10,
    description: 'desc 1',
    currency: 'euro',
    formattedReleaseDate: '10/10/2020',
    units: 3,
  },
  {
    id: '9i',
    title: 'Ticket 2',
    type: 'show',
    price: 5,
    description: 'desc 2',
    currency: 'euro',
    formattedReleaseDate: '10/10/2020',
    units: 2,
  },
];

describe('getCart use case', () => {
  it('build cart', () => {

    const result = getCart(tickets);

    expect(result).toEqual({
      total: 70,
      orderedTickets: [
        {
          id: '7u',
          title: 'Ticket 1',
          units: 3,
          price: 10,
          totalPrice: 30,
          currency: 'euro'
        },
        {
          id: '9i',
          title: 'Ticket 2',
          units: 2,
          price: 5,
          totalPrice: 10,
          currency: 'euro'
        },
        {
          id: '2b',
          title: 'Ticket 3',
          units: 1,
          price: 30,
          totalPrice: 30,
          currency: 'euro'
        }
      ],
    });
  })
})