import { Cart, CartItem } from "../Cart";
import { TicketItem } from "../Ticket"

const getCart = (tickets: TicketItem[]): Cart => {
  const orderedTickets: CartItem[] = [];
  let total = 0;

  tickets
    .filter((ticket: TicketItem) => ticket.units)
    .forEach((ticket: TicketItem) => {

      const subtotal = ticket.price * ticket.units;

      orderedTickets.push({
        id: ticket.id,
        title: ticket.title,
        units: ticket.units,
        price: ticket.price,
        totalPrice: ticket.price * ticket.units,
        currency: ticket.currency
      })

      total = total + subtotal;
    })

  orderedTickets.sort((ticket1, ticket2) => ticket2.units - ticket1.units);

  return {
    total,
    orderedTickets
  }
}

export { getCart }