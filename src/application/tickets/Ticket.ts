interface TicketItem {
  id: string;
  title: string;
  type: string;
  price: number;
  description: string;
  currency: string;
  formattedReleaseDate: string;
  units: number;
}

export type { TicketItem }