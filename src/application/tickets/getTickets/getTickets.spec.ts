import { TicketAPI } from "@/infra/domain";
import { ticketService } from "@/infra/services/Ticket.service";
import { Application } from "../..";
import { getTickets } from "./getTickets";
import { TicketItem } from "../Ticket";

const mockTickets: TicketAPI[] = [
  {
    id: "1a",
    title: "Johnny Cash tribute",
    type: "show",
    releaseDate: 1555970400000,
    description: "Mauris finibus commodo malesuada. Vestibulum porttitor, massa a gravida faucibus, augue velit tristique libero, sit amet scelerisque tortor erat ut velit. Praesent orci tellus, aliquam id felis vitae, laoreet facilisis.",
    price: 15,
    currency: "euro"
  },
  {
    id: "7p",
    title: "action for happinness",
    type: "talk",
    releaseDate: 1634680800000,
    description: "Donec facilisis quis risus ut blandit. Morbi iaculis vel nisi ut cursus. In vel imperdiet odio, imperdiet mollis diam. Nulla vulputate orci arcu, sed tincidunt est tempor et. Aenean at.",
    price: 9,
    currency: "euro"
  },
  {
    id: "2e",
    title: "vegan for beginners",
    type: "talk",
    releaseDate: 1538344800000,
    description: "Curabitur eget magna dui. Nam risus ligula, sagittis eget malesuada eu, blandit eget urna. Sed dignissim ante id quam feugiat, eget dictum libero sollicitudin. Aliquam a urna nec leo efficitur facilisis.",
    price: 8.5,
    currency: "euro"
  }
];

describe('getTickets use case', () => {

  let ticketServiceSpy: jest.SpyInstance;
  let getSavedTicketsSpy: jest.SpyInstance;

  beforeEach(() => {
    ticketServiceSpy = jest.spyOn(ticketService, 'getTickets');
    ticketServiceSpy.mockReturnValue(Promise.resolve(mockTickets));

    getSavedTicketsSpy = jest.spyOn(Application, 'getSavedTickets');
  })

  it('get tickets from api with no cart saved', async () => {
    
    getSavedTicketsSpy.mockReturnValue({});
  
    const tickets = await getTickets();
    const result: TicketItem[] = [
      {
        id: "7p",
        title: "action for happinness",
        type: "talk",
        formattedReleaseDate: "20/10/2021",
        description: "Donec facilisis quis risus ut blandit. Morbi iaculis vel nisi ut cursus. In vel imperdiet odio, imperdiet mollis diam. Nulla vulputate orci arcu, sed tincidunt est tempor et. Aenean at.",
        price: 9,
        currency: "euro",
        units: 0
      },
      {
        id: "1a",
        title: "Johnny Cash tribute",
        type: "show",
        formattedReleaseDate: "23/04/2019",
        description: "Mauris finibus commodo malesuada. Vestibulum porttitor, massa a gravida faucibus, augue velit tristique libero, sit amet scelerisque tortor erat ut velit. Praesent orci tellus, aliquam id felis vitae, laoreet facilisis.",
        price: 15,
        currency: "euro",
        units: 0
      },
      {
        id: "2e",
        title: "vegan for beginners",
        type: "talk",
        formattedReleaseDate: "01/10/2018",
        description: "Curabitur eget magna dui. Nam risus ligula, sagittis eget malesuada eu, blandit eget urna. Sed dignissim ante id quam feugiat, eget dictum libero sollicitudin. Aliquam a urna nec leo efficitur facilisis.",
        price: 8.5,
        currency: "euro",
        units: 0
      },
    ]

    expect(tickets).toEqual(result)
  }) 

  it('get tickets form API with cart saved', async () => {
    getSavedTicketsSpy.mockReturnValue({
      '7p': 4,
      '2e': 1,
    });
  
    const tickets = await getTickets();
    const result: TicketItem[] = [
      {
        id: "7p",
        title: "action for happinness",
        type: "talk",
        formattedReleaseDate: "20/10/2021",
        description: "Donec facilisis quis risus ut blandit. Morbi iaculis vel nisi ut cursus. In vel imperdiet odio, imperdiet mollis diam. Nulla vulputate orci arcu, sed tincidunt est tempor et. Aenean at.",
        price: 9,
        currency: "euro",
        units: 4
      },
      {
        id: "1a",
        title: "Johnny Cash tribute",
        type: "show",
        formattedReleaseDate: "23/04/2019",
        description: "Mauris finibus commodo malesuada. Vestibulum porttitor, massa a gravida faucibus, augue velit tristique libero, sit amet scelerisque tortor erat ut velit. Praesent orci tellus, aliquam id felis vitae, laoreet facilisis.",
        price: 15,
        currency: "euro",
        units: 0
      },
      {
        id: "2e",
        title: "vegan for beginners",
        type: "talk",
        formattedReleaseDate: "01/10/2018",
        description: "Curabitur eget magna dui. Nam risus ligula, sagittis eget malesuada eu, blandit eget urna. Sed dignissim ante id quam feugiat, eget dictum libero sollicitudin. Aliquam a urna nec leo efficitur facilisis.",
        price: 8.5,
        currency: "euro",
        units: 1
      },
    ];

    expect(tickets).toEqual(result)
  })
})
