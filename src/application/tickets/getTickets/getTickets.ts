import { Ticket } from "@/domain";
import { TicketAPI } from "@/infra/domain";
import { ticketService } from "@/infra/services/Ticket.service";
import { Application } from "../..";
import { TicketItem } from "../Ticket";

const getTickets = async (): Promise<TicketItem[]> => {
  const ticketsAPI = await ticketService.getTickets();
  const lastCart = Application.getSavedTickets();

  if (!ticketsAPI) {
    return [];  
  }

  const tickets = ticketsAPI.map((item: TicketAPI) => new Ticket(item));

  tickets.sort((ticket1: Ticket, ticket2: Ticket) => ticket2.releaseDate - ticket1.releaseDate);

  return  [...tickets].map((ticket: Ticket) => {
    
    const savedUnits = lastCart[ticket.id];

    return {
      id: ticket.id,
      title: ticket.title,
      type: ticket.type,
      price: ticket.price,
      description: ticket.description,
      currency: ticket.currency,
      formattedReleaseDate: ticket.getFormattedReleaseDate(),
      units: savedUnits || 0
    }
  });

}

export { getTickets }