import { storageService } from "@/infra/services/Storage.service";
import { cartStorageKey } from "../../config";
import { CartItem } from "../Cart";

const saveTickets = (tickets: CartItem[]) => {

  const ticketsObject: Record<string, number> = {};

  tickets.forEach((ticket) => {
    ticketsObject[ticket.id] = ticket.units;
  })

  storageService.set(cartStorageKey, ticketsObject);
}

export { saveTickets }