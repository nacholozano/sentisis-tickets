import { storageService } from "@/infra/services/Storage.service"
import { cartStorageKey } from "../../config";
import { saveTickets } from "./saveTickets";
import { CartItem } from "../Cart";

describe('saveTickets use case', () => {
  it('save cart', () => {

    const ticekts: CartItem[] = [
      {
        id: '1c',
        title: '',
        price: 10,
        currency: '',
        units: 5,
        totalPrice: 50
      },
      {
        id: '6y',
        title: '',
        price: 10,
        currency: '',
        units: 1,
        totalPrice: 10
      }
    ];

    const storageServiceSpy = jest.spyOn(storageService, 'set');

    saveTickets(ticekts);

    expect(storageServiceSpy).toHaveBeenCalledWith(
      cartStorageKey,
      {
        '1c': 5,
        '6y': 1
      }
    );
  })
})