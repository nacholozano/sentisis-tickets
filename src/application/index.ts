import { getTickets } from './tickets/getTickets/getTickets';
import { saveTickets } from './tickets/saveTickets/saveTickets';
import { getSavedTickets } from './tickets/getSavedTickets/getSavedTickets';
import { getCart } from './tickets/getCart/getCart';

const Application = {
  getTickets,
  saveTickets,
  getSavedTickets,
  getCart
}

export { Application }