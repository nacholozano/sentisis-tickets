class StorageService {

  get(key: string) {
    return JSON.parse(localStorage.getItem(key));
  }

  set(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

}

const storageService = new StorageService();

export { storageService }