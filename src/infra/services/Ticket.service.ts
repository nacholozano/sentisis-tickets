import { TicketAPI } from "@/infra/domain";
import { HttpClient } from "../httpClient";

class TicketService {

  private httpClient: HttpClient;

  constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  getTickets(): Promise<TicketAPI[]> {
    return this.httpClient.get<TicketAPI[]>('tickets');
  }

}

const httpClient = new HttpClient('https://my-json-server.typicode.com/davidan90/demo/');
const ticketService = new TicketService(httpClient);

export { ticketService }