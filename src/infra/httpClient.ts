class HttpClient {

  private baseUrl: string;

  constructor(baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  async get<T>(url: string): Promise<T> {
    try {
      const response = await fetch(this.baseUrl + url);

      if (response.status !== 200) {
        return Promise.resolve(null);
      }

      const data = await response.json();

      return data;

    } catch(e) {
      return Promise.resolve(null);
    }
  }
}

export { HttpClient }