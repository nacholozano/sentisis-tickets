interface TicketAPI {
  id: string,
  title: string,
  type: string,
  releaseDate: number,
  description: string,
  price: number,
  currency: string,
}

export type { TicketAPI }