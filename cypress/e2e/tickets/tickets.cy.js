/// <reference types="cypress" />

// Helper selector to find row
const getRow = (numberRow) => {
  return `table>tbody>tr:nth-child(${numberRow})`;
}

// Helper selectors to find cell
const getCell = (row, numberCell) => {
  return cy.get(`${row}>td:nth-child(${numberCell})`);
}

const selectors = {
  table: {
    decreaseButton: '[data-testid=unit-decrease]',
    input: '[data-testid=unit-selector-input]',
    increaseButton: '[data-testid=unit-increase]',
  },
  detail: {
    title: '[data-testid=title]',
    type: '[data-testid=type]',
    description: '[data-testid=description]',
    addItemButton: '[data-testid=add-item-detail-button]'
  },
  cart: {
    title: '[data-testid=title]',
    units: '[data-testid=units]',
    price: '[data-testid=price]',
    subtotal: '[data-testid=subtotal]',
    total: '[data-testid=total]',
    modalOverlay: '.ReactModal__Overlay',
    modalButton: '[data-testid=cart-button]'
  }
};

describe('load tickets', () => {

  beforeEach(() => {
    cy.intercept('GET', 'https://my-json-server.typicode.com/davidan90/demo/tickets', { fixture: 'tickets.json' } )
    cy.clearLocalStorage();
    cy.visit('http://localhost:3000/')
  })

  it('see tickets', () => {
    
    cy.get('table')
      .should('be.visible')
    
    const row = getRow(4);

    getCell(row, 1).contains('ghost')
    getCell(row, 2).contains('musical')
    getCell(row, 3).contains('10/12/2020')
    getCell(row, 5).contains('25 euro')
    
  })

  it('see ticket detail', () => {

    // open detail
    cy.get(getRow(4))
      .click()

    // Check text
    cy.get(selectors.detail.title)
      .contains('ghost')

    cy.get(selectors.detail.type)
      .contains('musical')

    cy.get(selectors.detail.description)
      .contains('Maecenas porta orci interdum lacus varius, at egestas turpis consequat. Mauris ac condimentum metus. Aenean viverra interdum porta. Nunc laoreet a libero sed finibus. Suspendisse vitae dictum magna. In ut elementum.')

  })

  it('add ticket from detail', () => {
    cy.get(getRow(4)).as('row4')
    
    // open detail
    cy.get('@row4').click()
    cy.get('@row4').get(selectors.detail.addItemButton).as('addItemButton')

    // click add button
    cy.get('@addItemButton')
      .click()

    cy.get('@row4').click()

    cy.get('@addItemButton')
      .click()

    const row = getRow(4);

    // Check value
    getCell(row, 4)
      .find(selectors.table.input)
      .should('have.value', 2)
  })

  it('remove units from unit selector', () => {
    const row = getRow(4);

    getCell(row, 4).as('unitSelectorCell')

    cy.get('@unitSelectorCell')
      .find(selectors.table.input)
      .type(4)

    // click decrease
    cy.get('@unitSelectorCell')
      .find(selectors.table.decreaseButton)
      .click()

    // check input
    cy.get('@unitSelectorCell')
      .find(selectors.table.input)
      .should('have.value', 3)
  })

  it('add units from unit selector', () => {
    const row = getRow(4);

    getCell(row, 4).as('unitSelectorCell')

    cy.get('@unitSelectorCell')
      .find(selectors.table.input)
      .type(4)

    // click increase
    cy.get('@unitSelectorCell')
      .find(selectors.table.increaseButton)
      .click()

    // check input
    cy.get('@unitSelectorCell')
      .find(selectors.table.input)
      .should('have.value', 5)
  })

  it('save ticket after reload', () => {
    const row4 = getRow(4);
    const row5 = getRow(5);

    getCell(row4, 4).find(selectors.table.input).as('unitSelectorCell4')
    getCell(row5, 4).find(selectors.table.input).as('unitSelectorCell5')

    // Write units
    cy.get('@unitSelectorCell4')
      .type(2)

    cy.get('@unitSelectorCell5')
      .type(1)

    // Reload
    cy.reload()

    // Check inputs
    cy.get('@unitSelectorCell4')
      .should('have.value', 2)

    cy.get('@unitSelectorCell5')
      .should('have.value', 1)

  })

  it('see cart', () => {
    const row2 = getRow(2);

    // set items
    getCell(row2, 4)
      .find(selectors.table.input)
      .type(2)

    const row5 = getRow(5);

    getCell(row5, 4)
      .find(selectors.table.input)
      .type(1)

    cy.get(selectors.cart.modalButton)
      .click()

    // Check metallica row

    cy.get('[data-testid=cart-row-6t]').as('metallicaRow')

    cy.get('@metallicaRow')
      .find(selectors.cart.title)
      .contains('metallica')

    cy.get('@metallicaRow')
      .find(selectors.cart.units)
      .contains('2')

    cy.get('@metallicaRow')
      .find(selectors.cart.price)
      .contains('50')

    cy.get('@metallicaRow')
      .find(selectors.cart.subtotal)
      .contains('100')

    // Check johnny Cash row

    cy.get('[data-testid=cart-row-1a]').as('cashTribute')

    cy.get('@cashTribute')
      .find(selectors.cart.title)
      .contains('Johnny Cash tribute')

    cy.get('@cashTribute')
      .find(selectors.cart.units)
      .contains('1')

    cy.get('@cashTribute')
      .find(selectors.cart.price)
      .contains('15')

    cy.get('@cashTribute')
      .find(selectors.cart.subtotal)
      .contains('15')
  })

  it('hide cart modal', () => {
    const row5 = getRow(5);

    getCell(row5, 4)
      .find(selectors.table.input)
      .type(1)

    cy.get(selectors.cart.modalButton)
      .click()

    //  Hide modal
    cy.get(selectors.cart.modalOverlay)
      .click()

    cy.get(selectors.cart.modalOverlay)
      .should('not.exist')
  })

});