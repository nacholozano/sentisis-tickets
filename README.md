# Sentisis test

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Required version

Node: `16.17.0`  
NPM: `8.15.0`

## Install dependencies

`npm i`

## Start project

1. `npm start`  
2. Open [http://localhost:3000](http://localhost:3000)

## Run unit test

`npm test`

## Run e2e test

App must be running.

1. `npm start`
2. `npm run cypress:run`

## Architecture

I have used hexagonal architecture with four main folders:
* infra
* domain
* application
* ui

### Infra
App external resources like http request and storage.  
We use these resources throught services.  
This folder also includes API types.

### Domain
These classes are a middleware between external resources and application use cases and contains domain utilities. 

### Application
This folder contains all use cases of our application.  
we retrieve necessary data for each use case and pass it to user interface.

### Ui
Components, pages and styles.  
Pages get data from application use cases and pass it down to componentes.
CSS modules are used to style components.

## Other notes

* There is no commit history because I always do this kind of test in one go
* This architecture can be improved by creating interfaces for each service, then injecting them into use cases. This way we can replace services with new ones easily but I think is too much for this test
* I use react-modal and mock the wrapper component for testing
* Some test are missing but I think you can get an idea of my testing knowledge